<?php

namespace Jean\SearchRss\Tests;

use PHPUnit\Framework\TestCase;
use Jean\SearchRss\SearchRss;

class SearchRssTest extends TestCase
{
    protected function setUp(): void
    {
        //
    }

    public function testGetNews()
    {
        $searchRss = new SearchRss();

        $result = $searchRss->getNews('esportes.xml');

        $this->assertCount(24, $result);
    }
}