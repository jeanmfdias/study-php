<?php

namespace Jean\SearchRss;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class SearchRss
{
    private string $baseUri;

    public function __construct()
    {
        $this->baseUri = 'https://www.gazetadopovo.com.br/feed/rss/';
    }

    public function getNews(string $url): array
    {
        $client = new Client(
            [
                'base_uri' => $this->baseUri
            ]
        );

        $response = $client->get($url);

        $xml = $response->getBody();

        return $this->readNews($xml);
    }

    private function readNews($xml): array
    {
        $items = [];

        $crawler = new Crawler();

        $crawler->addXmlContent($xml);

        $titles = $crawler->filterXpath('//rss/channel/item/title');

        foreach ($titles as $title) {
            $items[] = $title->textContent;
        }

        return $items;
    }
}
