<?php

namespace Jean\Tests;

use Jean\OO\Account;
use Jean\OO\Owner;

class AccountTest {

    public function testCreateAccount()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.456.789-10", "Felipe Fulano");
        $newAccount = new Account($newOwner, 1000);

        var_dump($newAccount);
    }

    public function testCreateAccountWithoutBalance()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.456.789-11", "Felipe Siclano");
        $newAccount = new Account($newOwner);

        var_dump($newAccount);
    }

    public function testAccountWithdraw()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $newAccount = new Account($newOwner, 1000);

        $newAccount->withdraw(200);

        var_dump($newAccount->getBalance() == 800);
    }

    public function testAccountWithdrawWithoutBalance()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.456.789-11", "Felipe Siclano");
        $newAccount = new Account($newOwner);
        $currentBalance = $newAccount->getBalance();

        $newAccount->withdraw(200);
        
        var_dump($newAccount->getBalance() == $currentBalance);
    }

    public function testAccountDeposit()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $newAccount = new Account($newOwner, 1000);

        $newAccount->deposit(200);

        var_dump($newAccount->getBalance() == 1200);
    }

    public function testAccountDepositNegativeValue()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $newOwner = new Owner("123.456.789-11", "Felipe Siclano");
        $newAccount = new Account($newOwner);
        $currentBalance = $newAccount->getBalance();

        $newAccount->deposit(-200);
        
        var_dump($newAccount->getBalance() == $currentBalance);
    }

    public function testAccountTransfer()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $firstOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $newAccount = new Account($firstOwner, 1000);

        $secondOwner = new Owner("123.567.456-45", "Felipe Siclano");
        $secondAccount = new Account($secondOwner, 100);

        $newAccount->transfer(500, $secondAccount);

        var_dump($newAccount->getBalance() == 500);
        var_dump($secondAccount->getBalance() == 600);
    }

    public function testAccountTransferWithoutBalance()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $firstOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $newAccount = new Account($firstOwner, 1000);

        $secondOwner = new Owner("123.567.456-45", "Felipe Siclano");
        $secondAccount = new Account($secondOwner, 100);

        $newAccount->transfer(5000, $secondAccount);

        var_dump($newAccount->getBalance() == 1000);
        var_dump($secondAccount->getBalance() == 100);
    }

    public function testAccountToString()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $firstOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $newAccount = new Account($firstOwner, 1000);

        var_dump($newAccount->toString());
    }

    public function testGetAccountNumber()
    {
        echo "----------" . __FUNCTION__ . "\n";

        $firstOwner = new Owner("123.123.123-10", "Marcelo Fulano");
        $firstAccount = new Account($firstOwner, 1000);
        $firstOwner = new Owner("123.123.123-11", "Marcelo Siclano");
        $secondAccount = new Account($firstOwner, 1000);

        var_dump(Account::getAccountNumber() == 2);
    }

}