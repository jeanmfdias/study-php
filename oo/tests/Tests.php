<?php

namespace Jean\Tests;

use Jean\Tests\AccountTest;
use Jean\Tests\OwnerTest;

$accountTest = new AccountTest();
$ownerTest = new OwnerTest();

$accountTest->testCreateAccount();
$accountTest->testCreateAccountWithoutBalance();
$accountTest->testAccountWithdraw();
$accountTest->testAccountWithdrawWithoutBalance();
$accountTest->testAccountDeposit();
$accountTest->testAccountDepositNegativeValue();
$accountTest->testAccountTransfer();
$accountTest->testAccountTransferWithoutBalance();
$accountTest->testAccountToString();
$accountTest->testGetAccountNumber();

$ownerTest->testCreateAccountValidate();
$ownerTest->testCreateAccountValidateFails();