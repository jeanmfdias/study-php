<?php

namespace Jean\Tests;

use Jean\OO\Owner;

class OwnerTest
{
    public function testCreateAccountValidate()
    {
        echo "----------" . __FUNCTION__ . "\n";
        $firstOwner = new Owner("123.123.123-10", "Marcelo Fulano");

        var_dump($firstOwner->validate());
    }

    public function testCreateAccountValidateFails()
    {
        echo "----------" . __FUNCTION__ . "\n";

        try {
            new Owner("123.123.123-101", "Ma");
            var_dump(false);
        } catch (\Exception $e) {
            var_dump(true);
        }
    }
}