<?php

namespace Jean\OO;

class Owner
{
    private string $id;
    private string $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;

        if (!$this->validate()) {
            throw new \Exception("Error validate");
        }
    }

    public function validate()
    {
        if (!$this->validateId()) {
            return false;
        }
        if (!$this->validateName()) {
            return false;
        }
        return true;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    private function validateName(): bool
    {
        if (strlen($this->name) < 5) {
            return false;
        }
        return true;
    }

    private function validateId(): bool
    {
        $id = str_replace(['.', '-'], '', $this->id);
        if (strlen($id) != 11) {
            return false;
        }
        return true;
    }
}