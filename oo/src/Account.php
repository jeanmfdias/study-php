<?php

namespace Jean\OO;

use Jean\OO\Owner;

class Account
{
    private float $balance;
    private Owner $owner;

    private static int $accountNumber = 0;

    public function __construct(Owner $owner, float $balance = 0) {
        $this->balance = $balance;
        $this->owner = $owner;

        self::$accountNumber++;
    }

    public function __destruct()
    {
        self::$accountNumber--;
    }

    public function withdraw(float $value): bool
    {
        if ($value <= $this->balance) {
            $this->balance -= $value;
            return true;
        }
        return false;
    }

    public function deposit(float $value): bool
    {
        if ($value > 0) {
            $this->balance += $value;
            return true;
        }
        return false;
    }

    public function transfer(float $value, Account $account): bool
    {
        if ($value > 0) {
            $result = $this->withdraw($value);
            if ($result) {
                $result = $account->deposit($value);
                if ($result) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function toString(): string
    {
        return "ID: " . $this->owner->getId() .
            "\nName: " . $this->owner->getName() .
            "\nBalance: " . $this->getBalance();
    }

    public static function getAccountNumber(): int
    {
        return self::$accountNumber;
    }
}